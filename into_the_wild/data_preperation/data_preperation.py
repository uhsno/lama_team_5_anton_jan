from segment_anything import SamPredictor, sam_model_registry
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import time
import os
from random import randint, seed

# enable import modules from parent folder
if __name__ == "__main__":
    import sys
    from pathlib import Path
    sys.path.append(str(Path(__file__).parent.parent))

# import config


def move_cwd(dir='into_the_wild'):
    current_cwd = os.getcwd()

    index = current_cwd.find(dir)
    if index != -1:
        new_cwd = current_cwd[:index+len(dir)]
        os.chdir(new_cwd)
    else:
        print(f'directory {dir} is no parent directory of {current_cwd}')

# checks if pixel is green
def is_green(pixel):
    if 10 < pixel[0] < 85 and 40 < pixel[1] and 40 < pixel[2]:
        return True
    else:
        return False

# checks if pixel is definetly not green
def is_definetly_not_green(pixel):
    if 100 < pixel[0] and 40 < pixel[1] and 40 < pixel[2]:
        return True
    else:
        return False


class bg_remover:
    def __init__(self, init_sam=True) -> None:
        sam_checkpoint_path = 'sam/sam_vit_h_4b8939.pth'

        self.sam = None

        if init_sam:
            t1 = time.time()
            print('init SAM...')
            # init SAM
            try:
                self.sam = sam_model_registry['vit_h'](checkpoint=sam_checkpoint_path)
            except FileNotFoundError:
                self.sam = sam_model_registry['vit_h'](checkpoint='data_preperation/' + sam_checkpoint_path)

            # use cpu if gpu is incompatible with cuda
            try:
                print('init GPU...')
                self.sam.to(device='cuda')
                print('using GPU')
            except AssertionError:
                print('GPU unavaillable -> using CPU')
                self.sam.to(device='cpu')

            self.predictor = SamPredictor(self.sam)
            print(f'init took {round(time.time()-t1, 3)}s')
        else:
            print('not initializing SAM')

        self.input_point = []
        self.input_label = []

    # marks some pixels as part of the leaf and definetly not part of the leaf
    def mark_leaf(self, image):
        blur_level = 20
        step_size = 15
        min_green_spots = 10

        self.input_point = []
        self.input_label = []

        image_hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)  # convert image to hsv
        image_hsv_b = cv.blur(image_hsv, (blur_level, blur_level))  # blur image to reduce noise

        for x in range(step_size, len(image)-1, step_size):
            for y in range(step_size, len(image[0])-1, step_size):
                if is_green(image_hsv_b[x][y]):
                    self.input_point.append([y, x])
                    self.input_label.append(1)

                elif is_definetly_not_green(image_hsv_b[x][y]):
                    self.input_point.append([y, x])
                    self.input_label.append(0)

        if self.input_label.count(1) < min_green_spots:
            self.input_point = [[int(len(image)/2), int(len(image[0])/2)]]
            self.input_label = [1]
        
        self.input_point = np.array(self.input_point)
        self.input_label = np.array(self.input_label)

    # generates multiple possible masks
    def generate_masks(self, image):
        # generate points on leaf
        self.mark_leaf(image)

        # generate & return masks
        self.predictor.set_image(image)
        return self.predictor.predict(point_coords=self.input_point, point_labels=self.input_label,
                                      multimask_output=True, )

    # returns mask with highes score
    def generate_best_mask(self, image):
        # close holes in mask with flood fill
        def close_holes_ff(mask, image):
            # make continuous loop around image 
            for i in range(len(mask)-1):
                mask[i][0] = False
                mask[0][i] = False
                mask[i][len(mask)-1] = False
                mask[len(mask)-1][i] = False
            
            mask_inv = np.logical_not(mask)      # invert mask

            image_size = len(image)-2
            seed_points = [[1, 1], [1, image_size], [image_size, image_size], [image_size, 1]]  # ff startpoints in corners

            possible_fill = None

            # start floodfill from a point at the edge which is not part of the mask
            for seed_point in seed_points:
                if mask_inv[seed_point[0]][seed_point[1]]:
                    possible_fill = cv.floodFill(np.uint8(mask_inv * 255), None, seed_point, False)
                    break
            
            # combine hole fixes and mask with holes
            if possible_fill:
                new_mask = mask | possible_fill[1].astype(bool)
                return new_mask
            else:
                # print('\nno possible mask')
                return mask
        
        def close_holes_morph(mask):
            mask = mask.astype(np.uint8)*255

            diameter_hole_kernel = 15

            if diameter_hole_kernel % 2 == 0:
                diameter_hole_kernel += 1

            # Create a square kernel with zeros
            kernel = np.zeros((diameter_hole_kernel, diameter_hole_kernel), dtype=np.uint8)

            # Calculate the center point
            center = diameter_hole_kernel // 2

            # Fill the kernel with 1s inside the circle
            for x in range(diameter_hole_kernel):
                for y in range(diameter_hole_kernel):
                    if (x - center)**2 + (y - center)**2 <= center**2:
                        kernel[x, y] = 1
            # close holes
            mask = cv.morphologyEx(mask, cv.MORPH_CLOSE, kernel)
            mask = mask.astype(bool)
            return mask


        # generate masks
        masks, scores, logits = self.generate_masks(image)
        # get mask with highest score
        mask = masks[np.argmax(scores)]

        # close holes in mask
        mask = close_holes_morph(mask)
        mask = close_holes_ff(mask, image)

        # make int mask from bool mask
        mask = mask.astype(np.uint8)*255

        # blur mask to smooth edges
        mask = cv.blur(mask, (5, 5))

        _, binary_mask = cv.threshold(mask,  100, 255, cv.THRESH_BINARY)

        # return mask with best score
        return binary_mask

    # isolates masked part from image
    def isolate(self, image):
        mask_bool = self.generate_best_mask(image)

        # convert mask type
        three_channel_mask = cv.cvtColor(mask_bool, cv.COLOR_GRAY2RGB)

        # remove unmasked part of the image
        masked_image = cv.bitwise_and(image, three_channel_mask)
        return masked_image

    # cuts out whole folder of images
    def isolate_folder(self, dir_path, new_path, max_files=None, overwrite_existing=True):
        min_leaf_size = 1/8     # minimum size of remaining image for not beeing discarded


        os.makedirs(new_path, exist_ok=True)

        # skip folder if number of images match
        if not overwrite_existing and len(os.listdir(dir_path)) == len(os.listdir(new_path)):
            print('skipped')
            return

        num_files = len(os.listdir(dir_path))
        t_start = time.time()
        t_stop = time.time()

        skipped_files = 0

        # loop over directory
        for n, image_name in enumerate(os.listdir(dir_path)):
            # skip image processing if image already exists
            if os.path.exists(new_path+'/'+image_name) and not overwrite_existing:
                skipped_files += 1
                continue

            # print progress info
            if max_files:
                print(
                    f'processing img {n+1}/{max_files} | {round(100 * (n+1) / max_files, 2)}% |',
                    f'elapsed: {int(t_stop - t_start)}s |',
                    f'eta: {int(((t_stop - t_start) / (n - skipped_files + 1)) * (max_files - n))}s | ',
                    f't/image: {round(float((t_stop - t_start) / (n - skipped_files + 1)), 3)}s',
                    ' ' * 20, end='\r')
            else:
                print(
                    f'processing img {n+1}/{num_files} | {round(100 * (n+1) / num_files, 2)}% |',
                    f'elapsed: {int(t_stop - t_start)}s |',
                    f'eta: {int(((t_stop - t_start) / (n - skipped_files + 1)) * (num_files - n))}s | ',
                    f't/image: {round(float((t_stop - t_start) / (n - skipped_files + 1)), 3)}s',
                    ' ' * 20, end='\r')

            # load image 
            image_path = dir_path + '/' + image_name
            image = cv.imread(image_path)

            # cut out image
            new_image = self.isolate(image)

            # validate image
            non_black_pixels_count = np.count_nonzero(np.all(new_image != [0, 0, 0], axis=2))

            # check if image contains enough of the leaf
            if non_black_pixels_count < len(new_image)*len(new_image[0])*min_leaf_size:
                # discard image
                cv.imwrite('data/discarded/' + image_name, new_image)
            else:
                # save new image
                cv.imwrite(new_path + '/' + image_name, new_image)

            t_stop = time.time()

            # stop if max_files is set & reached
            if max_files:
                if n >= max_files:
                    break

        # preserve last progress info print
        print()


# prepare an image for ML prediction
def prep_input_image(image, remover=None):
    img_size_target = 256

    # if not remover:
    #     remover = bg_remover()

    # correct aspect ratio
    if len(image[0])/len(image[1]) != 1:
        half_size = int(min(image.shape)/2)
        center_x = int(len(image)/2)
        center_y = int(len(image[0])/2)

        image = image[center_y - half_size:center_y + half_size, center_x - half_size:center_x + half_size]

    # resize
    if len(image) != img_size_target:
        image = cv.resize(image, (img_size_target, img_size_target), interpolation=cv.INTER_AREA)

    return image


# ==== debugging shit ====================================================

def load_random_image(base_path, seed_=None):
    seed(seed_)
    dir_content = os.listdir(base_path)
    image = cv.imread(base_path + '/' + dir_content[randint(0, len(dir_content))])
    image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    return image

# highlight classified zones
def show_green_zone(image):
    blur_level = 20

    # convert RGB image to HSV & split into components
    image_hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)
    H, S, V = cv.split(image_hsv)

    # blur & split
    image_hsv_b = cv.blur(image_hsv, (blur_level, blur_level))
    H_b, S_b, V_b = cv.split(image_hsv_b)

    # add all different hues to top line of the image
    for n, i in enumerate(H[0]):
        H_b[0][n] = n % 179
        S_b[0][n] = 125
        V_b[0][n] = 125
        if n > 180:
            H[0][n] = 0

    # loop over all pixels  &  set S and V according to classification
    for x, line in enumerate(H_b):
        for y, pixel_h in enumerate(line):
            if is_green(image_hsv_b[x][y]):
                S[x][y] = 255
                V[x][y] = 255
            if is_definetly_not_green(image_hsv_b[x][y]):
                S[x][y] = 0
                V[x][y] /= 4

    # merge image back together and convert to RGB
    image_hsv = cv.merge([H, S, V])
    image = cv.cvtColor(image_hsv, cv.COLOR_HSV2RGB)

    return image

# plot mask
def show_mask(mask, ax, random_color=False):
    if random_color:
        color = np.concatenate([np.random.random(3), np.array([0.6])], axis=0)
    else:
        color = np.array([30 / 255, 144 / 255, 255 / 255, 0.6])
    h, w = mask.shape[-2:]
    mask_image = mask.reshape(h, w, 1) * color.reshape(1, 1, -1)
    ax.imshow(mask_image)

# plot marked points
def show_points(coords, labels, ax, marker_size=100):
    pos_points = coords[labels == 1]
    neg_points = coords[labels == 0]

    for n, p in enumerate(coords):
        if labels[n] == 1:
            ax.text(p[0], p[1], str(n), color='blue')
        else:
            ax.text(p[0], p[1], str(n), color='red')

    # ax.scatter(pos_points[:, 0], pos_points[:, 1], color='green', marker='*', s=marker_size, edgecolor='white',
    #            linewidth=0.25)
    # ax.scatter(neg_points[:, 0], neg_points[:, 1], color='red', marker='*', s=marker_size, edgecolor='white',
    #            linewidth=0.25)

# ========================================================================


if __name__ == '__main__' and False:
    move_cwd()

    r = bg_remover(init_sam=False)
    # image = load_random_image(seed_=16)
    image = cv.imread('data/plant_diseases_dataset/valid/Potato___Early_blight/902d81fd-7e78-44b6-b71b-efa61e018c65___RS_Early.B 8100.JPG')

    image_marked = show_green_zone(image)
    r.mark_leaf(image)

    # for n, p in enumerate(r.input_point):
    #     print(n, p, r.input_label[n], cv.cvtColor(image, cv.COLOR_RGB2HSV)[p[0]][p[1]])

    plt.figure(figsize=(10, 10))
    plt.imshow(image_marked)
    show_points(r.input_point, r.input_label, plt.gca())
    plt.show()

# generate mask of random image & show it
if __name__ == '__main__' and False:
    move_cwd()

    r = bg_remover()

    # image = load_random_image('data/dataset_cutout/valid/Potato___Early_blight', 56994)
    image = cv.imread('data/plant_diseases_dataset/test/TomatoEarlyBlight2.JPG')

    print('generating mask...')
    mask = r.generate_best_mask(image)
    
    np.savetxt('test/mask.txt', mask)

    print('done')

    # image = show_green_zone(image)

    # plot stuff
    print('plot stuff...')
    plt.figure(figsize=(10, 10))
    plt.imshow(image)
    show_mask(mask.astype(np.bool_), plt.gca())
    # show_points(r.input_point, r.input_label, plt.gca())
    plt.axis('off')
    plt.show()

# cut out one image and save
if __name__ == '__main__' and False:
    move_cwd()

    remover = bg_remover()

    image = cv.imread('data/plant_diseases_dataset/test/TomatoEarlyBlight2.JPG')

    new_image = remover.isolate(image)

    cv.imwrite('data/test.png', new_image)

# cut out whole directory
if __name__ == '__main__' and False:
    move_cwd()

    print('init...')
    t1 = time.time()
    remover = bg_remover()
    print(f'init took {time.time() - t1}s')

    remover.isolate_folder('data/plant_diseases_dataset/train/Grape___Black_rot', 'data/removed_bg/Grape___Black_rot', max_files=10)

# cut out everything, but resumable
if __name__ == '__main__' and True:
    move_cwd()

    def go_deeper(path):
        dir_content = os.listdir(path)

        # if directory contains files -> process them
        if os.path.isfile(path+'/'+dir_content[0]):
            print('processing', path, 
                  f'| {os.listdir(Path(path).parent).index(os.path.basename(path))}/{len(os.listdir(Path(path).parent))}')
            remover.isolate_folder(path, path.replace(path_dataset_in, path_dataset_out), overwrite_existing=False)

        # if not -> go one layer deeper
        else:
            for folder in dir_content:
                go_deeper(path+'/'+folder)
        

    remover = bg_remover()
    

    path_base = 'data/'
    path_dataset_in = path_base + 'plant_diseases_dataset'
    path_dataset_out = path_base + 'dataset_cutout'

    go_deeper(path_dataset_in)

# test prep_input_image
if __name__ == '__main__' and False:
    move_cwd()

    image = prep_input_image(cv.imread('data/field_data/grape_esca.JPG'))

    cv.imshow('img', image)
    cv.waitKey(0)
    cv.destroyAllWindows()
