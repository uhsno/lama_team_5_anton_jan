import os

# set root path depending on IDE setup
if "into_the_wild" not in os.getcwd(): 
    BASE_DIR = "into_the_wild/"
else:
    BASE_DIR = ""

# unmodified dataset
DATA_DIR = BASE_DIR + "data/plant_diseases_dataset"
TRAIN_DIR = DATA_DIR + "/train"
VALID_DIR = DATA_DIR + "/valid"
TEST_DIR =  DATA_DIR + "/test"

# prepared dataset
CUT_DATA_DIR = BASE_DIR + "data/dataset_cutout"
CUT_TRAIN_DIR = CUT_DATA_DIR + "/train"
CUT_VALID_DIR = CUT_DATA_DIR + "/valid"
CUT_TEST_DIR =  CUT_DATA_DIR + "/test"

# GPU or CPU object
import torch
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

### attention, depending on pretrained model, a different transform may be necessary
### e.g. EfficientNet_B0_Weights.IMAGENET1K_V1.transforms

TRAINED_MODEL_FOLDER = BASE_DIR + "model/trained/"
TRAINED_MODEL_PATH_FORMAT = TRAINED_MODEL_FOLDER + "{filename}.pth"
