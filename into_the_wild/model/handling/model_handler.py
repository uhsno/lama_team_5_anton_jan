import matplotlib.pyplot as plt

from fastai.vision.all import *

# enable import modules from parent folder
if __name__ == "__main__":
    import sys
    from pathlib import Path
    sys.path.append(str(Path(__file__).parent.parent))
    sys.path.append(str(Path(__file__).parent.parent.parent))

import config

from torchvision.models import resnet50

from fastai.vision.all import *
from fastai.data.all import *
import timm
from functools import partial
from fastai.learner import *

from fastai.test_utils import *
from fastai.basics import *

from visualisation import *

import random
import glob



def file_output(text:str, file_name="test_out", path=str(Path(__file__).parent)+"/"):
    file_name = file_name + ".txt"

    with open(path + file_name, "w") as file:
        file.write(text)
        print(f"Output written to:\n    {path + file_name}")

# ideas: what is best loss function, what is berst optimizer usw,


def find_best_learning_rate(model=None, model_name="efficientnet_b0", resize=224, device=config.DEVICE):
    print(f'finding learn rate for {model_name}')

    since = time.time()

    dls : DataLoaders = ImageDataLoaders().from_folder(config.DATA_DIR, item_tfms=Resize(resize), device=device)
    if not model:
        model = partial(timm.create_model, model_name=model_name, pretrained=True)
    
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=config.TRAINED_MODEL_FOLDER)
    learn.model.to(config.DEVICE)
    print()
    file_output(str(learn.lr_find()), file_name="best_learning_rate_"+model_name)
    print(f"elapsed seconds: {time.time() - since}")


def train_from_pretrained(pretrained_name="efficientnet_b0", dataset_path=config.DATA_DIR,\
                          resize=224, epochs=10, freeze_epochs=2, base_lr=0.0017):
    
    dataset_name = dataset_path.replace(config.BASE_DIR, '').replace('data', '', 1).replace('/', '')

    # preparation
    dls : DataLoaders = ImageDataLoaders().from_folder(dataset_path, item_tfms=Resize(resize), device=config.DEVICE, shuffle_train=True)
    model = partial(timm.create_model, model_name=pretrained_name, pretrained=True)
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent, model_dir="trained")
    learn.model.to(config.DEVICE)

    # defining how the model is named when saved
    model_name = pretrained_name + "_" + dataset_name + "_" + str(time.strftime("%b%d_%H-%M", time.localtime()))
    
    # output model information: architecture, optimizer, loss function
    output_line = f"Based on {pretrained_name}\n"
    output_line += f'trained for {epochs} epochs & {freeze_epochs} freeze epochs\n'
    file_output(output_line + learn.summary(), model_name + "_info", path=str(Path(__file__).parent.parent)+"/trained/")

    # training
    # learn.fine_tune(epochs=epochs, freeze_epochs=freeze_epochs, base_lr=base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=model_name, with_opt=True)])

    print('processing freeze epochs')
    learn.freeze()
    learn.fit_one_cycle(freeze_epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=model_name, with_opt=True)])

    with open(str(Path(__file__).parent.parent)+"/trained/"+model_name + "_recorder_val.pkl", 'wb') as file:
        pickle.dump(learn.recorder.values, file)

    print('processing epochs')
    learn.unfreeze()
    learn.fit_one_cycle(epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=model_name, with_opt=True)])

    with open(str(Path(__file__).parent.parent)+"/trained/"+model_name + "_recorder_val.pkl", 'rb') as file:
        freeze_epoch_stats = pickle.load(file)
    # save training history
    with open(str(Path(__file__).parent.parent)+"/trained/"+model_name + "_recorder_val.pkl", 'wb') as file:
        pickle.dump([freeze_epoch_stats, learn.recorder.values], file)


def compute_results():
    pretrained_name = "efficientnet_b0"

    # preparation, this all needs to be done to get original learner back
    dls : DataLoaders = ImageDataLoaders().from_folder(config.DATA_DIR, item_tfms=Resize(224), device=config.DEVICE)
    model = partial(timm.create_model, model_name=pretrained_name)
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent, model_dir="trained")
    
    learn.load("efficientnet_b0_Feb07_01-22", device=config.DEVICE)

    # test classification
    learn.show_results()
    plt.show()

    # show worst
    interp = Interpretation.from_learner(learn)
    interp.plot_top_losses(9, figsize=(15,10))
    plt.show()


def classify(images_directory="into_the_wild/data/plant_diseases_dataset/test"):
    # getting an image to classify
    field_data_dir = images_directory

    image_paths = glob.glob(field_data_dir + "/*")
    random_image_path = random.choice(image_paths)

    pretrained_name = "efficientnet_b0"
    # preparation, this all needs to be done to get original learner back
    dls : DataLoaders = ImageDataLoaders().from_folder(config.DATA_DIR, item_tfms=Resize(224), device=config.DEVICE)
    model = partial(timm.create_model, model_name=pretrained_name, pretrained=True)
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent, model_dir="trained")
    learn.load("efficientnet_b0_Feb07_01-22", device=config.DEVICE)

    prediction, _, _ = learn.predict(random_image_path)
    show_image(PILImage.create(random_image_path), title=f"Prediction: {prediction}, Path: {random_image_path}")
    plt.show()


def train_variants(freeze_epochs, epochs, dataset_path=config.CUT_DATA_DIR, base_lr=0.0017, resize=224, pretrained_name = "efficientnet_b0", model=None, device=config.DEVICE, save_every_model=True):
    # prepare directories and names
    dataset_name = dataset_path.replace(config.BASE_DIR, '').replace('data', '', 1).replace('/', '')

    model_name = pretrained_name + '_' + dataset_name + '_'
    freeze_model_name = model_name + 'freeze_'

    save_dir = config.TRAINED_MODEL_FOLDER + 'multi_' + pretrained_name + '_' + dataset_name + '_' + str(time.strftime("%b%d_%H-%M", time.localtime()))
    freeze_save_dir = save_dir + '/freeze'


    print(f'training: {pretrained_name} for {freeze_epochs} freez- and {epochs} normal epochs\n')
    
    # save start time
    start_time = time.time()

    # load data
    print(f'loading dataset from: {dataset_path}')
    print(f'path exists: {os.path.exists(dataset_path)}')
    dls : DataLoaders = ImageDataLoaders().from_folder(dataset_path, item_tfms=Resize(resize), device=device, shuffle_train=True)

    # prep model for training
    if not model:
        model = partial(timm.create_model, model_name=pretrained_name, pretrained=True)
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent.parent, model_dir=freeze_save_dir)
    learn.model.to(device)

    # ===== FROZEN TRAINING =====
    print()
    print('-train freeze layers-')
    
    # start frozen training
    learn.freeze()
    learn.fit_one_cycle(freeze_epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=freeze_model_name, every_epoch=True, with_opt=True)])

    # save stats from freeze training
    with open(str(Path(__file__).parent.parent.parent)+'/'+freeze_save_dir+'/freeze_stats.pkl', 'wb') as file:
        pickle.dump(learn.recorder.values, file)
        print(f'written to {file.name}')


    # ===== UNFROZEN TRAINING =====
    # == from base model ==
    print()
    print('-training base model-')
    learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent.parent, model_dir=save_dir)

    # start unfrozen training
    learn.unfreeze()
    learn.fit_one_cycle(epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=model_name, every_epoch=True, with_opt=True)])

    # save training stats
    with open(str(Path(__file__).parent.parent.parent)+'/'+save_dir+'/'+model_name+'_stats.pkl', 'wb') as file:
        pickle.dump(learn.recorder.values, file)
        print(f'written to {file.name}')
    
    # == from frozen models ==
    # get file names of all stages of the frozen trained model
    frozen_model_names = []
    for name in os.listdir(str(Path(__file__).parent.parent.parent) +'/'+ freeze_save_dir):
        if re.match(r'.*[.]pth$', name):
            frozen_model_names.append(name)
    frozen_model_names.sort()

    print(f'\nfound {len(frozen_model_names)} frozen trained models')
    for m in frozen_model_names:
        print('    '+m)

    # train every frozen trained stage for epochs
    for n, frozen_trained_name in enumerate(frozen_model_names):
        frozen_trained_name = frozen_trained_name.replace('.pth', '')
        print()
        print('-training', frozen_trained_name, f'{n+1}/{len(frozen_model_names)}-')

        learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent.parent, model_dir=save_dir)
        # load partially trained model
        learn.load('freeze/'+frozen_trained_name, with_opt=True)

        # start unfrozen training
        learn.unfreeze()
        learn.fit_one_cycle(epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=frozen_trained_name, every_epoch=save_every_model, with_opt=True)])

        # save training stats
        with open(str(Path(__file__).parent.parent.parent)+'/'+save_dir+'/'+frozen_trained_name+'_stats.pkl', 'wb') as file:
            pickle.dump(learn.recorder.values, file)
            print(f'written to {file.name}')
    
    # save elapsed time 
    elapsed_time = time.time() - start_time
    with open(save_dir+'/compute_time.txt', 'a') as file:
        file.write(str(elapsed_time))


def continue_training(epochs, multi_folder, continue_index, model=None, pretrained_name='', dataset_path=config.CUT_DATA_DIR, base_lr=0.0017, resize=224, device=config.DEVICE):
    # prepare directories and names
    dataset_name = dataset_path.replace(config.BASE_DIR, '').replace('data', '', 1).replace('/', '')

    save_dir = config.TRAINED_MODEL_FOLDER + multi_folder

    print(f'training: {pretrained_name} for {epochs} epochs, starting at frozen moder Nr. {continue_index}')
    
    # training loop
    for n, frozen_model in enumerate(os.listdir(config.TRAINED_MODEL_FOLDER+'/'+multi_folder+'/freeze')):
        frozen_trained_name = frozen_model.replace('.pth', '')

        if n<continue_index:
            continue

        print(f'training: {frozen_trained_name}, {n+1}/{len(os.listdir(save_dir+"/freeze"))-1}')

        if not model:
            model = partial(timm.create_model, model_name=pretrained_name, pretrained=True)

        dls : DataLoaders = ImageDataLoaders().from_folder(dataset_path, item_tfms=Resize(resize), device=device, shuffle_train=True)
        
        learn = vision_learner(dls, model, metrics=[accuracy, F1Score(average='macro')], path=Path(__file__).parent.parent.parent, model_dir=save_dir)
        # load partially trained model
        learn.load('freeze/'+frozen_trained_name, with_opt=True)

        # start unfrozen training
        learn.unfreeze()
        learn.fit_one_cycle(epochs, base_lr, cbs=[EarlyStoppingCallback(monitor="valid_loss", patience=2), SaveModelCallback(fname=frozen_trained_name, every_epoch=True, with_opt=True)])

        # save training stats
        with open(str(Path(__file__).parent.parent.parent)+'/'+save_dir+'/'+frozen_trained_name+'_stats.pkl', 'wb') as file:
            pickle.dump(learn.recorder.values, file)
            print(f'written to {file.name}')


# tries out all availlable GPUs until one with enough free memory is found and runs the function on it
def gpu_handler(function_to_run, *args, **kwargs):
    gpu_count = torch.cuda.device_count()
    print(f'running: {function_to_run.__name__}')
    print(gpu_count, 'GPUs availlable')

    try:
        for gpu in range(gpu_count):
            try:
                print(f'trying GPU{gpu} of {gpu_count}')
                function_to_run(*args, device=torch.device(f'cuda:{gpu}'), **kwargs)

            except torch.cuda.OutOfMemoryError:
                print(f'\n\nGPU{gpu} is out of memory\n')
                
            except RuntimeError as e:
                print(e)
                print(e.__traceback__)
                print('-'*50, '\n')
                if 'out of memory' in str(e):
                    print('retry after GPU cache cleanup')
                    try:
                        torch.cuda.empty_cache()
                        function_to_run(*args, device=torch.device(f'cuda:{gpu}'), **kwargs)
                    except (RuntimeError, torch.cuda.OutOfMemoryError) as e2:
                        print(f'retry unsuccessful\n{e2}')
                        print(e2.__traceback__)
                        
            else:
                # if no error occured
                print('finished')
                torch.cuda.empty_cache()
                break

            print('='*50, '\n')

        else:
            # if loop finished normally
            print('no GPU availlable')

    except RuntimeError as e:
        print('\n ---TASK TERMINATED--- \n')
        print(e)
        print(e.__traceback__)


if __name__ == "__main__" and False:
    train_from_pretrained(dataset_path=config.CUT_DATA_DIR, epochs=1, freeze_epochs=1)

if __name__ == "__main__" and False:
    # find_best_learning_rate(model=resnet50, resize=224, model_name="resnet50")
    # find_best_learning_rate(resize=240, model_name="efficientnet_b1")

    gpu_handler(find_best_learning_rate, resize=300, model_name="efficientnet_b3")

if __name__ == "__main__":
    # gpu_handler(train_variants, 5, 15, resize=224, pretrained_name="efficientnet_b0", base_lr=0.001737, save_every_model=False, dataset_path=config.DATA_DIR)
    # gpu_handler(train_variants, 5, 15, resize=240, pretrained_name="efficientnet_b1", base_lr=0.001445, save_every_model=False, dataset_path=config.DATA_DIR)
    # gpu_handler(train_variants, 5, 15, resize=260, pretrained_name="efficientnet_b2", base_lr=0.001737, save_every_model=False, dataset_path=config.DATA_DIR)
    gpu_handler(train_variants, 5, 15, resize=300, pretrained_name="efficientnet_b3", base_lr=0.0012022644514, save_every_model=False)


    # gpu_handler(train_variants, 5, 15, resize=224, pretrained_name="resnet50", model=resnet50, base_lr=0.0008317637, save_every_model=False, dataset_path=config.DATA_DIR)
    # gpu_handler(continue_training, 15, 'multi_resnet50_dataset_cutout_Feb10_13-54', 1, model=resnet50, pretrained_name='resnet50', base_lr=.00083176378393)
    # gpu_handler(continue_training, 15, 'multi_efficientnet_b3_dataset_cutout_Feb10_18-19', 2, pretrained_name='efficientnet_b3', resize=300)
