import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pickle
import os

# enable import modules from parent folder
if __name__ == "__main__":
    import sys
    from pathlib import Path
    sys.path.append(str(Path(__file__).parent.parent))
    sys.path.append(str(Path(__file__).parent.parent.parent))

import config

import seaborn as sns
import numpy as np
import re



def load_training_results(model_name):
    with open(config.TRAINED_MODEL_FOLDER+model_name+'_recorder_val.pkl', 'rb') as file:
        data = pickle.load(file)
    return data


def plot_results(data, model_name):
    data = np.array(data)

    plt.plot(range(1, len(data)+1), data, label=['accuracy', 'training loss', 'validation loss'])
    plt.legend()
    plt.xlabel('epochs')
    plt.title(model_name)

    # set x axis to only show integers
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))

    plt.show()


def load_multi(model_name):
    path = config.TRAINED_MODEL_FOLDER
    path += model_name
    path_freeze = path + '/freeze'

    # load data from freezed training
    with open(path_freeze+'/'+'freeze_stats.pkl', 'rb') as file:
        freeze_data = pickle.load(file)

    # load data from training
    dir_list = os.listdir(path)

    # find files which contain recorder data
    recorder_files = []
    for file in dir_list:
        if file.find('.pkl') != -1:
            recorder_files.append(file)

    # load data and save in 2D array[freeze_epoch][epoch]
    training_data = []
    for file in recorder_files:
        # models trained on frozen trained models
        if re.search(r"__[0-9]+_stats.pkl", file):
            training_type = file[file.find('__')+2:].replace('_stats.pkl', '')
            freeze_epochs = int(training_type)+1
        # model trained without frozen training
        elif re.search(r"__stats.pkl", file):
            freeze_epochs = 0

        # expand first dimension
        while len(training_data) <= freeze_epochs:
            training_data.append([])

        training_data[freeze_epochs] = freeze_data[:freeze_epochs]
        # load from file
        with open(path+'/'+file, 'rb') as f:
            training_data[freeze_epochs] += pickle.load(f)

    plot_data = []
    for freeze_epoch_num, freeze_epoch in enumerate(training_data):
        if freeze_epoch_num != 0:
            plot_data.append(freeze_data[:freeze_epoch_num])
        else:
            plot_data.append([])

        plot_data[freeze_epoch_num] += training_data[freeze_epoch_num]
    return plot_data


def plot_multi(data_set, model_name):
    base_model = model_name.replace('multi_', '')[:-len('_Feb10_11-48')]

    if 'dataset' in base_model:
            data_set_name = base_model[base_model.find('dataset'):]
    else:
        data_set_name = base_model[base_model.find('plant'):]

    base_model = base_model.replace(data_set_name, '').replace('_', ' ')
    base_model = base_model[:-1]

    path = config.TRAINED_MODEL_FOLDER
    path += 'plots/'+model_name

    os.makedirs(path)


    for freeze_epoch_num, data in enumerate(data_set):
        if not data:
            print('no data for f epoch', freeze_epoch_num)
            continue

        if len(data) <= freeze_epoch_num:
            print('only freeze data for f epoch', freeze_epoch_num)
            continue

        plt.plot(range(len(data)), data, label=['training loss', 'validation loss', 'accuracy', 'F1-score'])
        plt.legend()
        plt.xlabel('epochs')
        if freeze_epoch_num != 0:
            plt.axvline(freeze_epoch_num-0.5, color='red', linestyle='--')

        accuracys = [sublist[2] for sublist in data]

        # plot horizontal line at y=max accuracy
        plt.plot([0, len(data)-1], [max(accuracys), max(accuracys)], color='g', linestyle='dotted')
        # set title

        plt.title(f'{base_model}, trained on {data_set_name}\n{freeze_epoch_num} freeze epochs, max accuracy: {round(max(accuracys)*100, 2)}%')

        # set x axis to only show integers
        plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
        plt.grid(True)

        plt.tight_layout()

        plt.savefig(path+f'/{model_name}_{freeze_epoch_num}.png')
        plt.clf()
        print(f'written to {path+f"/{model_name}_{freeze_epoch_num}.png"}')

    # plot accuracy against initial amount of freeze trainings
    comp_data = []
    for data in data_set:
        accuracys = [sublist[2] for sublist in data]
        comp_data.append([])

        train_losses = [sublist[1] for sublist in data]
        comp_data[-1].append(train_losses[accuracys.index(max(accuracys))])

        val_losses = [sublist[1] for sublist in data]
        comp_data[-1].append(val_losses[accuracys.index(max(accuracys))])

        comp_data[-1].append(max(accuracys))

    plt.plot(range(len(comp_data)), comp_data, label=['training loss', 'validation loss', 'accuracy'])
    plt.legend()
    plt.xlabel('freeze epochs')

    accuracys = [sublist[2] for sublist in comp_data]
    plt.title(f'{base_model}, trained on {data_set_name}\nmax accuracy: {round(max(accuracys)*100, 2)}%, @ {accuracys.index(max(accuracys))} freeze epochs'+
              f'\nmin accuracy: {round(min(accuracys)*100, 2)}%, @ {accuracys.index(min(accuracys))} freeze epochs')
    plt.grid(True)
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))

    plt.tight_layout()

    plt.savefig(path+f'/{model_name}_freeze_compare.png')
    print(f'written to', path+f'/{model_name}_freeze_compare.png')
    plt.clf()


def cleanup_log_data(path):
    with open(path) as file:
        text = file.read()
    text = text.split('\n')
    
    last_line = ''
    for line in text:
        if not re.match(r'Epoch( )[0-9](/)[0-9]', line) and not re.match(r'(█)', line):
            if (last_line or line) and not(not line and re.match(r'^[0-9].*[0-9]( )*$', last_line)):
                print(line)
                last_line = line


def plot_multi_new(data_set, model_name):
    base_model = model_name.replace('multi_', '')[:-len('_Feb10_11-48')]

    if 'dataset' in base_model:
            data_set_name = base_model[base_model.find('dataset'):]
    else:
        data_set_name = base_model[base_model.find('plant'):]

    base_model = base_model.replace(data_set_name, '').replace('_', ' ')
    base_model = base_model[:-1]

    path = config.TRAINED_MODEL_FOLDER
    path += 'plots_2/'+model_name

    os.makedirs(path)


    for freeze_epoch_num, data in enumerate(data_set):
        if not data:
            print('no data for f epoch', freeze_epoch_num)
            continue

        if len(data) <= freeze_epoch_num:
            print('only freeze data for f epoch', freeze_epoch_num)
            continue

        val_losses = []
        train_losses = []
        accuracy = []
        f1 = []
        for i in data:
            train_losses.append(i[0])
            val_losses.append(i[1])
            accuracy.append(i[2])
            f1.append(i[3])

        fig, ax1 = plt.subplots()
        ax1.set_ylim([0, 0.4])

        ax2 = ax1.twinx()
        ax2.set_ylim([0.8, 1])

        ax1.plot(range(len(data)), train_losses, label='training loss', color='blue')
        ax1.plot(range(len(data)), val_losses, label='validation loss', color='orange')

        ax2.plot(range(len(data)), accuracy, label='accuracy', color='green')
        ax2.plot(range(len(data)), f1, label='F1-score', color='yellow')
        
        
        fig.legend(loc='upper right')
        plt.xlabel('epochs')
        if freeze_epoch_num != 0:
            ax2.axvline(freeze_epoch_num-0.5, color='red', linestyle='--')

        accuracys = [sublist[2] for sublist in data]

        # plot horizontal line at y=max accuracy
        ax2.plot([0, len(data)-1], [max(accuracys), max(accuracys)], color='g', linestyle='dotted')
        # set title

        plt.title(f'{base_model}, trained on {data_set_name}\n{freeze_epoch_num} freeze epochs, max accuracy: {round(max(accuracys)*100, 2)}%', loc='left')

        # set x axis to only show integers
        plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
        plt.grid(True)

        plt.subplots_adjust(top=0.85)

        # plt.tight_layout()

        plt.savefig(path+f'/{model_name}_{freeze_epoch_num}.png')
        plt.close()
        plt.clf()
        print(f'written to {path+f"/{model_name}_{freeze_epoch_num}.png"}')

    # plot accuracy against initial amount of freeze trainings
    comp_data = []
    for data in data_set:
        accuracys = [sublist[2] for sublist in data]
        comp_data.append([])

        train_losses = [sublist[1] for sublist in data]
        comp_data[-1].append(train_losses[accuracys.index(max(accuracys))])

        val_losses = [sublist[1] for sublist in data]
        comp_data[-1].append(val_losses[accuracys.index(max(accuracys))])

        comp_data[-1].append(max(accuracys))

    fig, ax1 = plt.subplots()
    ax1.set_ylim([0, 0.2])

    ax2 = ax1.twinx()
    ax2.set_ylim([0.9, 1])

    val_losses = []
    train_losses = []
    accuracy = []
    for i in comp_data:
        train_losses.append(i[0])
        val_losses.append(i[1])
        accuracy.append(i[2])

    ax1.plot(range(len(comp_data)), train_losses, label='train loss', color='blue')
    ax1.plot(range(len(comp_data)), val_losses, label='validation loss', color='orange')

    ax2.plot(range(len(comp_data)), accuracy, label='accuracy', color='green')

    fig.legend(loc='upper right')
    plt.xlabel('freeze epochs')

    accuracys = [sublist[2] for sublist in comp_data]
    plt.title(f'{base_model}, trained on {data_set_name}\nmax accuracy: {round(max(accuracys)*100, 2)}%, @ {accuracys.index(max(accuracys))} freeze epochs'+
              f'\nmin accuracy: {round(min(accuracys)*100, 2)}%, @ {accuracys.index(min(accuracys))} freeze epochs', loc='left')
    plt.grid(True)
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))

    plt.subplots_adjust(top=0.85)

    # plt.tight_layout()

    plt.savefig(path+f'/{model_name}_freeze_compare.png')
    print(f'written to', path+f'/{model_name}_freeze_compare.png')
    plt.close()
    plt.clf()


if __name__ == '__main__' and False:
    model_name = 'multi_resnet50_dataset_cutout_Feb10_16-17'

    data = load_multi(model_name)
    plot_multi(data, model_name)

if __name__ == '__main__' and False:
    cleanup_log_data('model/trained/multi_resnet50_dataset_cutout_Feb10_16-17/nohup.out')


if __name__ == '__main__':
    train_folder = os.listdir(config.TRAINED_MODEL_FOLDER)
    multi = []
    for e in train_folder:
        if e.find('multi_') != -1:
            multi.append(e)

    for n, model in enumerate(multi):
        print(f'\nprocessing {model} | {n+1}/{len(multi)}')
        try:
            data = load_multi(model)
            plot_multi_new(data, model)
        except FileNotFoundError:
            print('skipped')
