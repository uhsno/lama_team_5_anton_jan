import os;

# enable import modules from parent folder
if __name__ == "__main__":
    import sys
    from pathlib import Path
    sys.path.append(str(Path(__file__).parent.parent))

import config


# create global list of info for generall access

PLANT_NAMES = []
STATE_NAMES = []
CLASS_NAMES = []

def init() -> None:
    # get classes, unique plant names and unique diseases
    dir_names = os.listdir(config.TRAIN_DIR)
    dir_names.sort()
    for elem in dir_names:
        if elem.split("___")[0] not in PLANT_NAMES:
            PLANT_NAMES.append(elem.split("___")[0].replace("_", " "))

        if elem.split("___")[1] not in STATE_NAMES:
            STATE_NAMES.append(elem.split("___")[1].replace("_", " "))

        CLASS_NAMES.append(elem.replace("___", ": ").replace("_", " "))

init()

# utility functions

def create_info() -> str:
    string_lines = []
    indent = "  - "

    # get size of dataset
    string_lines.append(f"train data size: {len(config.train_folder)}")
    string_lines.append(f"valid data size: {len(config.valid_dl)}")
    string_lines.append("")

    # indented list of class names
    string_lines.append(f"{len(CLASS_NAMES)} classes:")
    string_lines.extend([indent + elem for elem in CLASS_NAMES])
    string_lines.append("")

    # indented list of plant names
    string_lines.append(f"{len(PLANT_NAMES)} unique plants:")
    string_lines.extend([indent + elem for elem in PLANT_NAMES])
    string_lines.append("")

    # indented list of diseases, excludes health
    string_lines.append(f"{len(STATE_NAMES)} unique diseases:")
    string_lines.extend([indent + elem for elem in STATE_NAMES if elem != "healthy"])
    string_lines.append("")

    # output result
    result = "\n".join(string_lines)
    return result
    
# TODO display total number of images, images per class, plot it
# TODO plot color histograms


# output the generated info

if __name__ == "__main__":
    # write output to txt file
    save_path = "into_the_wild/exploration/"
    file_name = "out.txt"
    with open(save_path + file_name, "w") as file:
        file.write(create_info())
        print(f"Explorer output written to:\n    {save_path + file_name}")