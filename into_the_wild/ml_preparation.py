import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
from tempfile import TemporaryDirectory
from tqdm import tqdm
import time

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.optim import lr_scheduler
import torch.nn.functional as F

import torchvision
import torchvision.transforms as transforms
from torchvision.utils import make_grid
from torchvision.datasets import ImageFolder
from torchsummary import summary

import logging


DATA_DIR = "into_the_wild/data/plant_diseases_dataset"
TRAIN_DIR = DATA_DIR + "/train"
VALID_DIR = DATA_DIR + "/valid"
TEST_DIR =  DATA_DIR + "/test"

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    
# for moving data to device (CPU or GPU)
def to_device(data, device):
    """Move tensor(s) to chosen device"""
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)

# for loading in the device (GPU if available else CPU)
class DeviceDataLoader():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            yield to_device(b, self.device)
        
    def __len__(self):
        """Number of batches"""
        return len(self.dl)

def data_preparation():
    print(colored("___DATA_PREPARATION___", "red"))
    train = ImageFolder(TRAIN_DIR, transform=transforms.ToTensor())
    valid = ImageFolder(VALID_DIR, transform=transforms.ToTensor())
    test = ImageFolder(TRAIN_DIR, transform=transforms.ToTensor())

    # Setting the seed value
    random_seed = 7
    torch.manual_seed(random_seed)

    batch_size = 32
    train_dl = DataLoader(train, batch_size, shuffle=True, num_workers=5, pin_memory=True)
    valid_dl = DataLoader(valid, batch_size, num_workers=5, pin_memory=True)
    test_dl = DataLoader(test, batch_size, num_workers=5, pin_memory=True)

    return train_dl, valid_dl, test_dl


def train_model(model, criterion, optimizer, scheduler, dataloaders, num_epochs=1):
    """function for training a model with given epochs

    Args:
        model (_type_): _description_
        criterion (_type_): _description_
        optimizer (_type_): _description_
        scheduler (_type_): _description_
        dataloaders ({}): dict of train and test dataloader
        num_epochs (int, optional): _description_. Defaults to 25.

    Returns:
        _type_: _description_
    """
    dataset_sizes = {x: (len(dataloaders[x])) for x in ["train", "test"]}

    since = time.time()

    # Create a temporary directory to save training checkpoints
    with TemporaryDirectory() as tempdir:
        best_model_params_path = os.path.join(tempdir, 'best_model_params.pt')

        torch.save(model.state_dict(), best_model_params_path)
        best_acc = 0.0

        for epoch in range(num_epochs):
            print(f'Epoch {epoch}/{num_epochs - 1}')
            print('-' * 10)

            # Each epoch has a training and validation phase
            for phase in ['train', 'test']:
                if phase == 'train':
                    model.train()  # Set model to training mode
                else:
                    model.eval()   # Set model to evaluate mode

                running_loss = 0.0
                running_corrects = 0

                # Iterate over data.
                for inputs, labels in tqdm(dataloaders[phase]):

                    # 1. transfer the inputs and labels to the device, before zeroing the gradients here

                    inputs = inputs.to(DEVICE)
                    labels = labels.to(DEVICE)
                    optimizer.zero_grad()
                    
                    # track history if only in train
                    with torch.set_grad_enabled(phase == 'train'):

                        # 2. caluclate the output, the argmax() of the output and the loss here

                        outputs = model(inputs)
                        _, preds = torch.max(outputs, 1)  # Get the index of the max log-probability
                        loss = criterion(outputs, labels)

                        # 3. Backward pass here and the step. Make sure that this is only happening in the training stage

                        if phase == 'train':
                            loss.backward()  # Backward pass to calculate gradients
                            optimizer.step()  # Update weights

                    # statistics
                    running_loss += loss.item() * inputs.size(0)
                    running_corrects += torch.sum(preds == labels.data)

                if phase == 'train':
                    scheduler.step()

                epoch_loss = running_loss / dataset_sizes[phase]
                epoch_acc = running_corrects.double() / dataset_sizes[phase]

                print(f'{phase} Loss: {epoch_loss:.4f} Acc: {epoch_acc:.4f}')

                # deep copy the model
                if phase == 'test' and epoch_acc > best_acc:
                    best_acc = epoch_acc
                    torch.save(model.state_dict(), best_model_params_path)

        time_elapsed = time.time() - since
        print(f'Training complete in {time_elapsed // 60:.0f}m {time_elapsed % 60:.0f}s')
        print(f'Best test Acc: {best_acc:4f}')

        # load best model weights
        model.load_state_dict(torch.load(best_model_params_path))
    return model

def training(dataloaders):
    model_ft = torchvision.models.resnet18().to(DEVICE)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    model = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, dataloaders, num_epochs=1, )
    model_ft_save_path = "/home/uhsno/lama_team_5_anton_jan/into_the_wild/models"

    return model

def imshow(inp, title=None):
    """Display image for Tensor."""

    # changing from (C,H,W) to (H,W,C)
    inp = inp.numpy().transpose((1, 2, 0))

    inp = np.clip(inp, 0, 1)
    if title is not None:
        plt.title(title)
    plt.axis("off")
    return plt.imshow(inp, cmap="gray")

def visualize_specific_sample(model, sample, data):
    was_training = model.training
    model.eval()
    images_so_far = 0
    fig = plt.figure()

    with torch.no_grad():
        input = data[sample][0]
        label = torch.tensor(data[sample][1])

        input_batched = input[None,:,:,:]
        input_batched = input_batched.to(DEVICE)
        label = label.to(DEVICE)

        outputs = model(input_batched)
        _, preds = torch.max(outputs, dim=1)

        # iterating over images contained in the batch
        images_so_far += 1
        ax = plt.subplot(1,1,1)
        ax.axis("off")
        ax.set_title(f'predicted: {os.listdir(TRAIN_DIR)[preds]}')
        imshow(input.cpu().data)

        model.train(mode=was_training)

def test_model(model, test_dl):
    sample_index = 0
    test_images, test_labels = next(iter(test_dl))
    visualize_specific_sample(model, sample_index, test_dl.dataset)

if __name__ == "__main__":
    print(colored("___START___", "green"))

    data_analysis()
    train_dl, valid_dl, test_dl = data_preparation()
    model = training({"train": train_dl, "test": valid_dl})
    test_model(model, test_dl)

    print(colored("___END___", "green"))
    