import numpy as np
import cv2 as cv
import os



def close_holes_ff(mask, image):
            mask_inv = np.logical_not(mask)      # invert mask

            # imshow(mask_inv)

            image_size = len(image)-2
            seed_points = [[1, 1], [1, image_size], [image_size, image_size], [image_size, 1]]  # ff startpoints in corners

            # start floodfil from a point at the edge which is not part of the mask
            for seed_point in seed_points:
                if mask_inv[seed_point[0]][seed_point[1]]:
                    possible_fill = cv.floodFill(np.uint8(mask_inv * 255), None, seed_point, False)
                    break
            
            # combine hole fixes and mask with holes
            if possible_fill:
                new_mask = mask | possible_fill[1].astype(bool)
                return new_mask
            else:
                print('no possible mask')
                return mask
            

def imshow(img):
    img = img.astype(np.uint8)*255
    cv.imshow('img', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


if __name__ == "__main__" and False:
    image = cv.imread('test/67eaa192-bbc0-4726-8d2a-61501a46f473.webp')

    image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    _, im_bool = cv.threshold(image,  125, 255, cv.THRESH_BINARY)
    im_bool = im_bool.astype(bool)


    image = close_holes_ff(im_bool, im_bool)

    imshow(image)
 

if __name__ == "__main__" and True:
     print(os.getcwd())
     print(os.listdir('data'))
