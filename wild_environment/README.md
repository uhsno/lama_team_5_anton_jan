### Importing the environment
In the terminal, run the command:
``` bash
conda env create --file environment_wild.yml
```
After the environment is created, you need to activate it using:
```bash
conda activate wild
```
Using VSCode, to activate the environment, press CTRL+SHIFT+P > "Select Interpreter" > and select Python Interpreter from environment.
If it isn't suggested, look for the environment in the .conda folder in your computer.

### Adding dependencies to the environment
To keep the environment portable across platforms, use the following command to create the yaml file
```bash
conda env export --from-history >> environment_wild.yml
```
For some reason, it ignores installations from pip, so pip packages need to be added manually. So which are installed using
``` bash
pip list --format=freeze > pip_packages.txt
```
and add the missing ones.