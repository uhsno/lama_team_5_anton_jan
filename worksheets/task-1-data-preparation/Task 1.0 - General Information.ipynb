{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# LAMA Task 1.0 - General Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Type Hints\n",
    "While Python does not usually enforce you to annotate which type a variable, function parameter, return value, etc. is, it can be very helpful to add those anyways to make code easier to read and understand. Python provides a standardized way of providing these type annotations, called [type hints](https://docs.python.org/3/library/typing.html). In this section, we'll give a short presentation of how type hints look, work, and how you can use them yourself.\n",
    "\n",
    "### Why use type hints?\n",
    "There is a multitude of reasons why you might want to use type hints. Some of these are:\n",
    "* A lot quicker to write than explicit documentation about the types: Instead of writing `some_variable = some_function() # some_variable is a list of integers`, you just write `some_variable: List[int] = some_function()`. The latter tends to be easier to read as there is a single standardized format of providing this information.\n",
    "* Automatically interpretable: If you use advanced development environments such as VS Code, they can interpret these type hints automatically and warn when you are mixing up types or just tell you what type a variable will be without executing the code.\n",
    "\n",
    "### How do we add them?\n",
    "In general, type hints are placed _behind_ the statement that they refer to. There are two syntaxes that you'll see: Whenever a type hint refers to a variable or function argument (in the function definition), the type hint will be added with a colon: `variable_or_argument_name: type`. For method return types, they are added at the end of the definition with an arrow: `def some_function() -> type:`\n",
    "\n",
    "### What do we write in place of `type`?\n",
    "The type name you write in place of `type` is the same as the class name. If you want to find out the class name for some value, you can just use the `type()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Type of strings:')\n",
    "print(type('some_string'))\n",
    "print('Type of integers:')\n",
    "print(type(42))\n",
    "print('Type of decimal numbers:')\n",
    "print(type(4.2))\n",
    "print('Type of boolean values:')\n",
    "print(type(True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, strings are called `str`, integer numbers are called `int`, decimal numbers are called `float`, and booleans values are called `bool`.\n",
    "\n",
    "We will also encounter other types from different libraries. For them, we write `library.type`. Two common type names you will encounter in future notebooks are `pd.DataFrame` and `np.ndarray`. `pd` and `np` are the abbreviations for numpy and pandas and `DataFrame` and `ndarray` refer to pandas data frames and numpy arrays respectively.\n",
    "\n",
    "### Advanced types\n",
    "If you are using tuples or lists, Python will tell you that they are of type `tuple` or `list`. Unfortunately, those don't tell us anything about what is in them - if we are expecting a list of integers specifically, we might also want to specify that. For that, Python provides a special type annotation syntax via the `typing` library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import * # Allows us to use the advanced type annotation syntax\n",
    "\n",
    "# For lists, we write the element type inside the square brackets. All elements are expected to be of the same type.\n",
    "# Note the uppercase 'L' in 'List': All of the advanced type annotations from the typing library use uppercase names\n",
    "a: List[int] = [1, 2, 3]\n",
    "\n",
    "# For tuples, each element type is specified separately\n",
    "b: Tuple[int, str, int] = (1, 'a', 2)\n",
    "\n",
    "# For dictionaries, the key type is specified as the first argument and the value type is specified as the second argument\n",
    "c: Dict[str, int] = { 'a': 1, 'b': 2, 'c': 42 }\n",
    "\n",
    "# You can also use advanced type hints inside other ones of course\n",
    "d: List[Tuple[int, int]] = [(1, 2), (3, 4), (5, 6)]\n",
    "\n",
    "# If you want to use the advanced syntax, but there's no single type that could be assigned to one parameter, you can use 'Any' instead\n",
    "e: Dict[str, Any] = { 'a': 1, 'b': True, 'c': 'Hello World' }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Additional advanced types (not used in LAMA)\n",
    "Of course, the three advanced type annotation types presented before are not all that are available. In case you are interested or want to use them yourself, here are some additional ones you might encounter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Unions mean that the type will be one of the specified ones\n",
    "f: Union[int, float, str] = 'asdf' # This can be either an int, float or string\n",
    "\n",
    "# For the special case of unions, where you either expect some type or 'None', you can also use Optional\n",
    "g: Optional[float] = 1.5 # Optional[float] is the same as Union[float, None]\n",
    "\n",
    "# If you don't necessarily want a list but just something that contains many integers or something similar which you can loop through using a for-in-loop, you can use Iterable:\n",
    "h: Iterable[int] = [1, 2, 3] # Could also be a tuple (1, 2, 3), a numpy array np.array([1, 2, 3]) or many other things\n",
    "\n",
    "# If you are passing a function, you specify them as 'Callable' with a list of the argument types and the return type\n",
    "def i_function(a: int, b: int) -> float:\n",
    "    return (a + b) / 2\n",
    "i: Callable[[int, int], float] = i_function\n",
    "i_result: float = i(1, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, there are even more possible type annotations you can use, but you'll rarely encounter them. For a complete documentation, you can refer to the [official Python typing documentation](https://docs.python.org/3/library/typing.html)."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "a244f739dd41aaf750592b2be5ff52b29b13d893664b047bdb054883f607fea9"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  },
  "metadata": {
   "interpreter": {
    "hash": "ac59ebe37160ed0dfa835113d9b8498d9f09ceb179beaac4002f036b9467c963"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
