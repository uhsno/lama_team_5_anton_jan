# LAMA_Team_5
Ich bin keine ReadMe, ich putze hier nur.

## Datensatz
https://www.kaggle.com/datasets/vipoooool/new-plant-diseases-dataset

## Gitlab Link
https://gitlab.kit.edu/uhsno/lama_team_5_anton_jan \
(Ist temporär auf public gestellt.)

## Kleiner Rundgang
Das Into The Wild Projekt befindet sich im gleichnamigen Verzeichnis.
### `data/`
Dieses Verzeichnis enthält den heruntergeladenen Datensatz und die in unserem Projekt aus dem Hintergrund herausgeschnittenen Blätter. Wegen der Dateigrößenbegrenzung sind die Daten nur auf GitLab und nicht in der Abgabe in ILIAS.

### `data_preparation`
Mit dem hier enthaltenen Code wurden mit SAM die Blätter aus dem Hintergrund herausgeschnitten.

### `exploration/`
Der Code in diesem Verzeichnis dient dazu allgemeine Informationen aus dem Datensatz zu extrahieren wie z.B. Anzahl der Klassen.

### `model/`
In diesem Verzeichnis fand das Training der Modelle statt. `handling.py` trainiert Modelle und legt diese dann in das `trained/` Verzeichnis ab. Trainierte Modelle sind aus Platzgründen ebenfalls nur auf GitLab!

### `config.py`
Diese Datei enthält Konstanten die global benötigt werden.

### Und der Rest?
`ml_preparation` und `test/` sind noch Reste vom Entwicklungsprozess.